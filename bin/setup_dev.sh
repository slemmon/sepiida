#!/bin/bash
name=${PWD##*/}
name=${name//[-._]/}

printf 'Setting up %s\n' $name

printf "...Creating databases\n"
dropdb ${name}
dropdb ${name}_test

createdb ${name}
createdb ${name}_test

psql -d ${name} -c "CREATE EXTENSION IF NOT EXISTS pgcrypto;"
psql -d ${name}_test -c "CREATE EXTENSION IF NOT EXISTS pgcrypto;"

psql -d ${name} -c "CREATE USER dev WITH PASSWORD 'authentise';"

psql -d ${name} -c "GRANT ALL PRIVILEGES ON DATABASE ${name} to dev;"
psql -d ${name}_test -c "GRANT ALL PRIVILEGES ON DATABASE ${name}_test to dev;"

printf "...Setting up configs\n"
mkdir -p ~/.authentise

ln -sf ${PWD}/config.json ~/.authentise/${name}.json
ln -sf ${PWD}/alembic.ini ~/.authentise/${name}_alembic.ini

if [ ! -d "/etc/authentise" ]; then
  echo "Could not find /etc/authentise.  Creating...\n"
  sudo ln -s  ~/.authentise /etc/authentise
fi

if [ ! -d "ve" ]; then
  pyvenv ve
fi

source ve/bin/activate

printf "...Installing Requirements\n"
pip install -e .[develop]

printf "...Migrating Database\n"
alembic --config /etc/authentise/${name}_alembic.ini  upgrade head

deactivate
