#!/bin/bash
PROJECT=sepiida
SCRIPTS=""
if [ -z "$1" ]; then
    FILES="$(find $PROJECT -maxdepth 3 -name "*.py" -not -path "*alembic/*" -print) $SCRIPTS $(find tests -maxdepth 3 -name "*.py" -print)"
else
    FILES="$1"
    echo "linting $FILES"
fi
mkdir -p dist
pylint $FILES | tee dist/pylint.log
