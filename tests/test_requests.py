import json
import logging

import pytest
import requests

import sepiida.requests
import sepiida.responses
import sepiida.session

LOGGER = logging.getLogger(__name__)

def test_auth_passthrough_session(app, configuration, httpretty, json_client, pao_user):
    @app.route('/test-request-passthrough/')
    def _do_passthrough(): # pylint: disable=unused-variable
        session = sepiida.requests.user_session()
        response = session.get("http://localhost/session-required/")
        return response.content, response.status_code

    def _session_required(request, dummy_uri, headers):
        _session_required.was_called = True
        assert 'session=' in request.headers['cookie']
        headers['Content-Type'] = 'application/json'
        return 200, headers, json.dumps({'success': True})
    _session_required.was_called = False

    httpretty.register_uri(
        httpretty.GET,
        "http://localhost/session-required/",
        body=_session_required
    )
    sepiida.session.register_session_handlers(app, configuration.pao_root)
    with json_client.session_transaction() as session:
        session['user_uri'] = pao_user['uri']
    response = json_client.get("/test-request-passthrough/")
    assert response.status_code == 200
    assert response.json == {"success": True}
    assert _session_required.was_called

def test_auth_passthrough_basic_auth(app, basic_auth, configuration, httpretty, json_client, pao_user):
    @app.route('/test-request-passthrough/')
    def _do_passthrough(): # pylint: disable=unused-variable
        session = sepiida.requests.user_session()
        response = session.get("http://localhost/session-required/")
        return response.content, response.status_code

    def _session_required(request, dummy_uri, headers):
        _session_required.was_called = True
        expected_auth = basic_auth(pao_user['username'], pao_user['password'])
        assert request.headers['Authorization'] == expected_auth
        headers['Content-Type'] = 'application/json'
        return 200, headers, json.dumps({'success': True})
    _session_required.was_called = False

    httpretty.register_uri(
        httpretty.GET,
        "http://localhost/session-required/",
        body=_session_required
    )
    sepiida.session.register_session_handlers(app, configuration.pao_root)
    response = json_client.get("/test-request-passthrough/", auth=(pao_user['username'], pao_user['password']))
    assert response.status_code == 200
    assert response.json == {"success": True}
    assert _session_required.was_called

def test_auth_privileged(app, basic_auth, configuration, httpretty, json_client, pao_user):
    privileged_url = "https://{}/session-required/".format(app.config['SERVER_NAME'])
    @app.route('/test-request-elevated/')
    def _do_elevated(): # pylint: disable=unused-variable
        session = sepiida.requests.privileged_session()
        response = session.get(privileged_url)
        return response.content, response.status_code

    def _session_required(request, dummy_uri, headers):
        _session_required.was_called = True
        expected_auth = basic_auth('api', configuration.api_token)
        assert request.headers['Authorization'] == expected_auth
        headers['Content-Type'] = 'application/json'
        return 200, headers, json.dumps({'success': True})
    _session_required.was_called = False

    httpretty.register_uri(
        httpretty.GET,
        privileged_url,
        body=_session_required
    )
    sepiida.session.register_session_handlers(app, configuration.pao_root, internal_domains=configuration.sepiida_internal_domains)
    response = json_client.get('/test-request-elevated/', auth=(pao_user['username'], pao_user['password']))
    assert response.status_code == 200
    assert response.json == {'success': True}
    assert _session_required.was_called

def test_user_session_basic_auth(basic_auth, httpretty):
    httpretty.register_uri(
        httpretty.GET,
        'http://localhost/test-endpoint/',
        status=200)
    session = sepiida.requests.user_session(username='foo', password='bar')
    response = session.get('http://localhost/test-endpoint/')
    assert response.status_code == 200
    assert httpretty.last_request().headers['Authorization'] == basic_auth('foo', 'bar')

def test_user_session_cookie(httpretty):
    httpretty.register_uri(
        httpretty.GET,
        'http://localhost/test-endpoint/',
        status=200)
    session = sepiida.requests.user_session(session='blahblahblah')
    response = session.get('http://localhost/test-endpoint/')
    assert response.status_code == 200
    assert httpretty.last_request().headers['Cookie'] == 'session=blahblahblah'

def test_privileged_session_with_api_token():
    api_token = 'secret-token'
    session = sepiida.requests.privileged_session(api_token)
    assert session.auth.password == api_token

def test_privileged_session_without_api_token(app):
    session = sepiida.requests.privileged_session()
    assert session.auth.password == app.config['API_TOKEN']

@pytest.mark.usefixtures('app')
@pytest.mark.parametrize('url, error', [
    ('https://www.stuff.com/123', sepiida.requests.InvalidPrivilegedDomain),
    ('http://www.stuff.com/123', sepiida.requests.InvalidPrivilegedScheme),
    ('asdf', requests.exceptions.MissingSchema),
    (None, requests.exceptions.MissingSchema),
])
def test_privileged_auth(url, error):
    session = sepiida.requests.privileged_session()
    with pytest.raises(error):
        session.get(url)
