import pytest

import sepiida.cors


@pytest.mark.parametrize("origin", [
    'http://www.example.com',
    'http://www.another-example.com',
    'http://app.dev-auth.com.s3-website-us-west-2.amazonaws.com',
])
def test_cors_base(app, json_client, origin):
    @app.route('/test-cors', methods=['GET'])
    def _handle_request(): # pylint: disable=unused-variable
        return "", 200
    assert sepiida.cors.register_cors_handlers(app, [
        'example.com',
        'another-example.com',
        'dev-auth.com.s3-website-us-west-2.amazonaws.com',
    ])

    response = json_client.get('/test-cors', headers={ 'origin': origin })
    assert response.headers['Access-Control-Allow-Origin'] == origin
    assert 'Access-Control-Allow-Credentials' not in response.headers
    assert response.headers['Access-Control-Expose-Headers'] == ", ".join(sepiida.cors.DEFAULT_EXPOSE_HEADERS)


@pytest.mark.parametrize("origin", ['http://www.example.com', 'http://www.another-example.com'])
def test_cors_supports_credentials(app, json_client, origin):
    @app.route('/test-cors', methods=['POST'])
    def _handle_request(): # pylint: disable=unused-variable
        return "", 201
    assert sepiida.cors.register_cors_handlers(app, app.config['TRUSTED_DOMAINS'], supports_credentials=True)

    response = json_client.post('/test-cors', json={'test': 'a'}, headers={ 'origin': origin })
    assert response.headers['Access-Control-Allow-Origin'] == origin
    assert response.headers['Access-Control-Allow-Credentials'] == 'true'


@pytest.mark.parametrize("origin", ['http://www.example.com', 'http://www.another-example.com'])
def test_cors_expose_headers(app, json_client, origin):
    @app.route('/test-cors', methods=['POST'])
    def _handle_request(): # pylint: disable=unused-variable
        return "", 201
    expose_headers = '*'
    assert sepiida.cors.register_cors_handlers(app, app.config['TRUSTED_DOMAINS'], expose_headers=expose_headers)

    response = json_client.post('/test-cors', json={'test': 'a'}, headers={ 'origin': origin })
    assert response.headers['Access-Control-Allow-Origin'] == origin
    assert response.headers['Access-Control-Expose-Headers'] == expose_headers


@pytest.mark.parametrize("origin", ['http://bad-example.com', 'http://www.bad-example.com'])
def test_invalid_cors(app, json_client, origin):
    @app.route('/test-cors', methods=['POST'])
    def _handle_request(): # pylint: disable=unused-variable
        return "", 201
    assert sepiida.cors.register_cors_handlers(app, app.config['TRUSTED_DOMAINS'])

    response = json_client.post('/test-cors', json={'test': 'a'}, headers={ 'origin': origin })
    assert 'access-control-allow-origin' not in response.headers


@pytest.mark.parametrize("origin", ['http://www.example.com', 'http://www.another-example.com'])
def test_cors_no_config(app, json_client, origin):
    @app.route('/test-cors', methods=['POST'])
    def _handle_request(): # pylint: disable=unused-variable
        return "", 201
    assert not sepiida.cors.register_cors_handlers(app)

    response = json_client.post('/test-cors', json={'test': 'a'}, headers={ 'origin': origin })
    assert 'access-control-allow-origin' not in response.headers
