import urllib.parse

import pytest

import sepiida.endpoints
import sepiida.fields
import sepiida.serialization
import tests.helpers


def basic_signature():
    return sepiida.fields.Object(s={
        'my_prop'   : sepiida.fields.String(),
    })

def double_signature():
    return sepiida.fields.Object(s={
        'field1'    : sepiida.fields.String(),
        'field2'    : sepiida.fields.String(),
    })

def test_encoding(app, client):
    tests.helpers.create_expected_payload_endpoint(app, basic_signature(), {'my_prop': 'foo'})

    response = client.post('/test-endpoint/', data={'my_prop': 'foo'})
    assert response.status_code == 204

@pytest.mark.parametrize('body, error', [
    ('flub',    'Expecting value: line 1 column 1 (char 0)'),
    ('{',       'Expecting property name enclosed in double quotes: line 1 column 2 (char 1)'),
    ('}',       'Expecting value: line 1 column 1 (char 0)'),
    (b'\xFF',   "Your request was not UTF-8 encoded: 'utf-8' codec can't decode byte 0xff in position 0: invalid start byte"),
])
def test_post_bad_json_payloads(app, body, client, error):
    tests.helpers.create_expected_payload_endpoint(app, basic_signature(), None)
    response = client.post('/test-endpoint/', data=body, content_type='application/json')
    assert response.status_code == 400
    expected = "Your payload specified a Content-Type of application/json but we were unable to decode it as such: " + error
    assert response.json['errors'][0]['title'] == expected

def test_post_bad_form_payloads(app, client):
    tests.helpers.create_expected_payload_endpoint(app, basic_signature(), None)
    response = client.post('/test-endpoint/', data=b'\xFF', content_type='application/x-www-form-urlencoded')
    assert response.status_code == 400
    expected = "You did not provide the my_prop property. It is required when POSTing to this resource"
    assert response.json['errors'][0]['title'] == expected

@pytest.mark.parametrize('response, expected', (
    ({'a': 'b'},                'a\r\nb\r\n'),
    ([{'a': 'b'}, {'a': 'c'}],  'a\r\nb\r\nc\r\n'),
    ({'a': 'foo', 'b': 'bar'},  'a,b\r\nfoo,bar\r\n'),
    ([{'a': 'foo', 'b': 'bar'}, {'a': 'biff', 'b': 'baff'}],
      'a,b\r\nfoo,bar\r\nbiff,baff\r\n'),
))
def test_csv_serialization_no_signature(response, expected):
    results = sepiida.serialization.dumps_csv(response, None)
    assert results == expected

def test_csv_serialization_list(app, client):
    payload = [{
        'field1': 'value11',
        'field2': 'value12',
    }, {
        'field1': 'value21',
        'field2': 'value22',
    }]
    tests.helpers.create_expected_payload_endpoint(app, double_signature(), payload)
    response = client.get('/test-endpoint/', headers={'Accept': 'text/csv'})
    assert response.status_code == 200
    data = response.data.decode('utf-8')
    assert data == 'field1,field2\r\nvalue11,value12\r\nvalue21,value22\r\n'

def test_csv_serialization_single(app, client):
    payload = {
        'field1': 'value11',
        'field2': 'value12',
    }
    tests.helpers.create_expected_payload_endpoint(app, double_signature(), payload)
    response = client.get('/test-endpoint/1/', headers={'Accept': 'text/csv'})
    assert response.status_code == 200
    data = response.data.decode('utf-8')
    assert data == 'field1,field2\r\nvalue11,value12\r\n'

def test_post_no_content_type(app, client):
    tests.helpers.create_expected_payload_endpoint(app, basic_signature(), None)
    response = client.post('/test-endpoint/', data={'my_prop': 'foo'}, content_type='')
    assert response.status_code == 400
    expected = "Your request did not include the 'Content-Type' header"
    assert response.json['errors'][0]['title'].startswith(expected)

def test_post_invalid_content_type(app, client):
    tests.helpers.create_expected_payload_endpoint(app, basic_signature(), None)
    response = client.post('/test-endpoint/', data={'my_prop': 'foo'}, content_type='invalid')
    assert response.status_code == 400
    expected = "The content-type header you specified, invalid, is not one that we support. Please try one of"
    assert response.json['errors'][0]['title'].startswith(expected)

def test_get_form_encoded(app, client):
    tests.helpers.create_expected_payload_endpoint(app, basic_signature(), {'my_prop': 'foo'})
    response = client.get('/test-endpoint/', headers={'Accept': 'application/x-www-form-urlencoded'})
    assert response.status_code == 200
    content = urllib.parse.parse_qs(response.data.decode('utf-8'))
    expected = {'resources[0].my_prop': ['foo']}
    assert content == expected

def test_list_form_encoded(app, client):
    tests.helpers.create_expected_payload_endpoint(app, basic_signature(), {'my_prop': 'foo'})
    response = client.get('/test-endpoint/0/', headers={'Accept': 'application/x-www-form-urlencoded'})
    assert response.status_code == 200
    assert response.data.decode('utf-8') == 'my_prop=foo'

@pytest.mark.parametrize('accept', [
    'application/json, application/x-www-form-urlencoded',
    'application/x-www-form-urlencoded;q=0.3, application/json',
    'application/x-www-form-urlencoded;q=0.3, application/json;q=0.7',
    'application/xml, application/json',
    '*/*',
])
def test_accept_negotiation(accept, app, client):
    tests.helpers.create_expected_payload_endpoint(app, basic_signature(), {'my_prop': 'foo'})
    response = client.get('/test-endpoint/0/', headers={'Accept': accept})
    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/json'

def test_default_response(app, client):
    tests.helpers.create_expected_payload_endpoint(app, basic_signature(), {'my_prop': 'foo'}, return_on_post=True)
    response = client.post('/test-endpoint/', data={'my_prop': 'foo'})
    assert response.status_code == 201
    assert response.headers['Content-Type'] == 'application/x-www-form-urlencoded'

@pytest.mark.parametrize('headers', [
    {'Accept': 'application/x-www-form-urlencoded'},
    {'Content-Type': 'application/x-www-form-urlencoded'},
])
def test_empty_response_content_type(app, client, headers):
    tests.helpers.create_expected_payload_endpoint(app, basic_signature(), None)
    response = client.get('/test-endpoint/', headers=headers)
    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/x-www-form-urlencoded'

@pytest.mark.parametrize('inp, expected', [
    ({'foo': 'bar'},            {'foo': 'bar'}),
    ({'foo': {'bar': 'baz'}},   {'foo.bar': 'baz'}),
    ({'foo': ['bar', 'baz']},   {'foo[0]': 'bar', 'foo[1]': 'baz'}),
    ({'foo': 1},                {'foo': 1}),
    ({'a': {'b': [1, 2]}},      {'a.b[0]': 1, 'a.b[1]': 2}),
    ({'a': [{'b': 'c'}]},       {'a[0].b': 'c'}),
])
def test_preprocess_form(inp, expected):
    output = sepiida.serialization.preprocess_form(inp)
    assert output == expected
