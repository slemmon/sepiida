import json
import uuid as UUID

import flask
import pytest

import sepiida.endpoints
import sepiida.errors
import sepiida.responses
import sepiida.session
from sepiida import fields


def test_uuid_endpoint(app, client):
    @app.route("/test-endpoint/<uuid:x>/")
    def _test_resource(x): # pylint: disable=unused-variable
        assert isinstance(x, UUID.UUID)
        return "", 200
    _uuid = UUID.uuid4()
    url = "/test-endpoint/{}/".format(_uuid)
    response = client.get(url)
    assert response.status_code == 200

    url = "/test-endpoint/invalid/"
    response = client.get(url)
    assert response.status_code == 404

def _setup_test_endpoint(app, configuration, public_methods=None):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        PUBLIC_METHODS = public_methods or []
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def list():
            return {'status': 'ok'}
        @staticmethod
        def get(_id): # pylint: disable=unused-argument
            return {'status': 'ok'}
    sepiida.endpoints.add_resource(app, TestEndpoint)
    sepiida.session.register_session_handlers(app, configuration.pao_root)

def test_endpoint_private(app, configuration, json_client):
    _setup_test_endpoint(app, configuration)

    response = json_client.get('/test-endpoint/1/')
    assert response.status_code == 403

def test_endpoint_public_list(app, configuration, json_client):
    _setup_test_endpoint(app, configuration, public_methods=['list'])

    response = json_client.get("/test-endpoint/")
    assert response.status_code == 200
    assert response.json == {'status': 'ok'}

    response = json_client.get("/test-endpoint/42/")
    assert response.status_code == 403

def test_endpoint_public_get(app, configuration, json_client):
    _setup_test_endpoint(app, configuration, public_methods=['get'])

    response = json_client.get("/test-endpoint/42/")
    assert response.status_code == 200
    assert response.json == {'status': 'ok'}

    response = json_client.get("/test-endpoint/")
    assert response.status_code == 403

def test_endpoint_invalid_public_method(app, configuration):
    with pytest.raises(Exception) as e:
        _setup_test_endpoint(app, configuration, public_methods=['retrieve'])

    assert str(e.value) == ("Method retrieve is not supported as PUBLIC_METHOD. "
                            "Supported methods are ['delete', 'get', 'list', 'post', 'put'].")

def test_endpoint_basic(app, configuration, json_client, json_client_session):
    _setup_test_endpoint(app, configuration)

    response = json_client.get('/test-endpoint/')
    assert response.status_code == 403

    response = json_client_session.get('/test-endpoint/')
    assert response.status_code == 200
    assert response.json == {'status': 'ok'}

def test_endpoint_fields_invalid(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = fields.Object(s={
            'status'    : fields.String(),
        })
        @staticmethod
        def list():
            return {'status': 'ok'}
    sepiida.endpoints.add_resource(app, TestEndpoint)

    response = json_client.get('/test-endpoint/?fields=invalid')
    assert response.status_code == 400
    expected = {'errors': [{
        'code'  : 'invalid-fields-query',
        'title' : ("You specified a fields value of 'invalid'. Fields must "
            "be specified as a comma-separated list of fields enclosed in '[' and ']'")
    }]}
    assert expected == response.json

def test_endpoint_fields_unrecognized(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = fields.Object(s={
            'status'    : fields.String(),
        })
        def list(self): # pylint: disable=no-self-use
            return {'status': 'ok'}
    sepiida.endpoints.add_resource(app, TestEndpoint)

    response = json_client.get('/test-endpoint/?fields=[unrecognized]')
    assert response.status_code == 400
    expected = {'errors': [{
        'code'  : 'invalid-field-specified',
        'title' : ("You specified the resource return 'unrecognized' but it is not a valid field for this resource. "
                   "Valid fields include 'status'"),
    }]}
    assert expected == response.json

    response = json_client.get('/test-endpoint/?fields=[unrecognized1,unrecognized2]')
    assert response.status_code == 400
    expected = {'errors': [{
        'code'  : 'invalid-field-specified',
        'title' : ("You specified the resource return 'unrecognized1' but it is not a valid field for this resource. "
                   "Valid fields include 'status'"),
    },{
        'code'  : 'invalid-field-specified',
        'title' : ("You specified the resource return 'unrecognized2' but it is not a valid field for this resource. "
                   "Valid fields include 'status'"),
    }]}
    assert expected == response.json

def test_endpoint_fields_sparse(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = fields.Object(s={
            'content'   : fields.String(),
            'status'    : fields.String(),
            'index'     : fields.Integer(),
        })
        @staticmethod
        def get(_id): # pylint: disable=unused-argument
            return {
                'content'   : 'some-content',
                'status'    : 'ok',
                'index'     : 0,
            }
    sepiida.endpoints.add_resource(app, TestEndpoint)

    response = json_client.get('/test-endpoint/1/')
    assert response.status_code == 200
    assert response.json == {'content': 'some-content', 'index': 0, 'status': 'ok'}

    response = json_client.get('/test-endpoint/1/?fields=[content,status]')
    assert response.status_code == 200
    assert response.json == {'content': 'some-content', 'status': 'ok'}

    response = json_client.get('/test-endpoint/1/?fields=[content, status]')
    assert response.status_code == 200
    assert response.json == {'content': 'some-content', 'status': 'ok'}

def test_endpoint_fields_no_calculate(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = fields.Object(s={
            'content'   : fields.String(),
            'status'    : fields.String(),
            'index'     : fields.Integer(),
        })
        def get(self, _id): # pylint: disable=unused-argument
            def _my_long_calculation():
                raise Exception("Should not be called")
            return {
                'content'   : 'some-content',
                'index'     : _my_long_calculation() if 'index' in self.fields else None,
                'status'    : 'ok'
            }
    sepiida.endpoints.add_resource(app, TestEndpoint)

    response = json_client.get('/test-endpoint/1/?fields=[content,status]')
    assert response.status_code == 200
    assert response.json == {'content': 'some-content', 'status': 'ok'}

@pytest.mark.parametrize('queryargs, code, title', [
    ['filter=foo', 'empty-filter-name',
        "You requested a filter with no name. Filter requests should be of the form 'filter[property]'"],
    ['filterBob=foo', 'invalid-filter-request',
        "You requested a filter 'filterBob'. Filter requests should be of the form 'filter[property]'"],
    ['filter[Bob=foo', 'invalid-filter-request',
        "You requested a filter 'filter[Bob'. Filter requests should be of the form 'filter[property]'"],
    ['filterBob]=foo', 'invalid-filter-request',
        "You requested a filter 'filterBob]'. Filter requests should be of the form 'filter[property]'"],
    ['filter[bob]=foo', 'invalid-filter-property',
        "Your requested filter property, 'bob', is not a valid property for this endpoint. Please choose one of 'content', 'status'"],
    ['filter[content]=foo&filter[content]=bar', 'filter-property-specified-more-than-once',
        "Your requested filter property, 'filter[content]', was specified more than once. We don't currently support that"],
])
def test_endpoint_filter_errors(app, json_client, queryargs, code, title):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = fields.Object(s={
            'content'   : fields.String(),
            'status'    : fields.String(),
        })
        def list(self): # pylint: disable=no-self-use
            assert False, "Should not reach this code on input {}".format(queryargs)

    sepiida.endpoints.add_resource(app, TestEndpoint)
    response = json_client.get('/test-endpoint/?' + queryargs)
    assert response.status_code == 400
    expected = {'errors': [{
        'code'  : code,
        'title' : title,
    }]}
    assert response.json == expected

def _sameish(one, two):
    """
    The ordering of our list items that is generated from our queryargs is undefined because of the way that flask handles
    queryargs. Because of that we have to be pretty lenient in our comparison when checking that the filters we get
    matches our expectations
    """
    for k, v in one.items():
        for x in v:
            assert x in two[k]
        assert len(two[k]) == len(v)
    for k, v in two.items():
        for x in v:
            assert x in one[k]
        assert len(one[k]) == len(v)
    return True

def _create_filter_endpoint(app):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = fields.Object(s={
            'x': fields.String(),
            'y': fields.String(),
        })
        def get(self, _id):
            assert _id
            result = {k: [{
                    'operation' : _filter.operation,
                    'values'    : _filter.values,
                } for _filter in v
            ] for k, v in self.filters.items()}
            return flask.make_response(json.dumps(result))
        def list(self):
            result = {k: [{
                    'operation' : _filter.operation,
                    'values'    : _filter.values,
                } for _filter in v
            ] for k, v in self.filters.items()}
            return flask.make_response(json.dumps(result))

    sepiida.endpoints.add_resource(app, TestEndpoint)


@pytest.mark.parametrize('queryargs, expected', [
    ('', {}),
    ('filter[x]=abc',
        {'x': [{'operation': '=', 'values': ['abc']}]}),
    ('filter[x]=abc,xyz',
        {'x': [{'operation': '=', 'values': ['abc', 'xyz']}]}),
    ('filter[x]=abc&filter[y]<=xyz',
        {'x': [{'operation': '=', 'values': ['abc']}], 'y': [{'operation': '<=', 'values': ['xyz']}]}),
    ('filter[x]<abc,123&filter[y]>=xyz',
        {'x': [{'operation': '<', 'values': ['abc', '123']}], 'y': [{'operation': '>=', 'values': ['xyz']}]}),
    ('filter[x]<abc&filter[x]>=xyz',
        {'x': [{'operation': '<', 'values': ['abc']}, {'operation': '>=', 'values': ['xyz']}]}),
])
def test_endpoint_filter_happy_path(app, expected, json_client, queryargs):
    _create_filter_endpoint(app)
    response = json_client.get('/test-endpoint/1/?' + queryargs)
    assert response.status_code == 200
    assert _sameish(response.json, expected)

@pytest.mark.parametrize('queryargs, expected', [
    ('', {}),
    ('filter[x]=abc',
        {'x': [{'operation': '=', 'values': ['abc']}]}),
    ('filter[x]=abc,xyz',
        {'x': [{'operation': '=', 'values': ['abc', 'xyz']}]}),
    ('filter[x]=abc&filter[y]<=xyz',
        {'x': [{'operation': '=', 'values': ['abc']}], 'y': [{'operation': '<=', 'values': ['xyz']}]}),
    ('filter[x]<abc,123&filter[y]>=xyz',
        {'x': [{'operation': '<', 'values': ['abc', '123']}], 'y': [{'operation': '>=', 'values': ['xyz']}]}),
    ('filter[x]<abc&filter[x]>=xyz',
        {'x': [{'operation': '<', 'values': ['abc']}, {'operation': '>=', 'values': ['xyz']}]}),
])
def test_endpoint_filter_resource_happy_path(app, expected, json_client, queryargs):
    _create_filter_endpoint(app)
    response = json_client.get('/test-endpoint/?' + queryargs)
    assert response.status_code == 200
    assert _sameish(response.json, expected)

@pytest.mark.parametrize('queryargs, expected', [
    ('', {}),
    ('filter[x]=abc',
        {'x': [{'operation': '=', 'values': ['abc']}]}),
    ('filter[x]=abc,xyz',
        {'x': [{'operation': '=', 'values': ['abc', 'xyz']}]}),
    ('filter[x]=abc&filter[y]<=xyz',
        {'x': [{'operation': '=', 'values': ['abc']}], 'y': [{'operation': '<=', 'values': ['xyz']}]}),
    ('filter[x]<abc,123&filter[y]>=xyz',
        {'x': [{'operation': '<', 'values': ['abc', '123']}], 'y': [{'operation': '>=', 'values': ['xyz']}]}),
    ('filter[x]<abc&filter[x]>=xyz',
        {'x': [{'operation': '<', 'values': ['abc']}, {'operation': '>=', 'values': ['xyz']}]}),
])
def test_endpoint_filter_in_body(app, expected, json_client, queryargs):
    _create_filter_endpoint(app)
    response = json_client.get('/test-endpoint/', data=queryargs)
    assert response.status_code == 200
    assert _sameish(response.json, expected)

def test_endpoint_filter_bad_body(app, json_client):
    _create_filter_endpoint(app)
    response = json_client.get('/test-endpoint/', data=bytes.fromhex('deadbeef'))
    assert response.status_code == 400
    assert response.json == {'errors': [{
        'code'  : 'bad-querystring-body',
        'title' : "'utf-8' codec can't decode byte 0xbe in position 2: invalid start byte",
    }]}

def test_endpoint_methods_list(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object(s={
            'content'   : fields.String(),
        })
        @staticmethod
        def list():
            return [{'content': 'a'}, {'content': 'b'}]
    sepiida.endpoints.add_resource(app, TestEndpoint)

    response = json_client.get('/test-endpoint/')
    assert response.status_code == 200
    assert response.json == {'resources': [{'content': 'a'}, {'content': 'b'}]}

    response = json_client.get('/test-endpoint/123/')
    assert response.status_code == 404

def test_endpoint_methods_get(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = fields.Object(s={
            'content'   : fields.String(),
        })
        @staticmethod
        def get(uuid=None, _id=None):
            assert uuid is None or isinstance(uuid, UUID.UUID)
            assert _id is None or isinstance(_id, int)
            return {'content': 'a'}
    sepiida.endpoints.add_resource(app, TestEndpoint)

    response = json_client.get('/test-endpoint/123/')
    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json == {'content': 'a'}

    response = json_client.get('/test-endpoint/{}/'.format(UUID.uuid4()))
    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json == {'content': 'a'}

    response = json_client.get('/test-endpoint/')
    assert response.status_code == 404

def _setup_put_endpoint(app, returns=None, signature=None):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = signature
        @staticmethod
        def put(uuid=None, _id=None):
            assert uuid is None or isinstance(uuid, UUID.UUID)
            assert _id is None or isinstance(_id, int)
            return returns
    sepiida.endpoints.add_resource(app, TestEndpoint)

def test_endpoint_methods_put_empty(app, json_client):
    _setup_put_endpoint(app)
    response = json_client.put('/test-endpoint/', json={'status': 1})
    assert response.status_code == 404

    response = json_client.put('/test-endpoint/123/', json={'status': 1})
    assert response.status_code == 204
    assert response.headers['Content-Type'] == 'text/plain'

def test_endpoint_methods_put_content(app, json_client):
    _setup_put_endpoint(app, returns={})
    response = json_client.put('/test-endpoint/123/', json={'status': 1})
    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json == {}

def test_endpoint_methods_put_no_content_disallowed(app, json_client):
    signature = sepiida.fields.Object(s={})
    _setup_put_endpoint(app, signature=signature)
    response = json_client.put('/test-endpoint/123/', json={})
    assert response.status_code == 400
    assert response.json == {'errors': [{
        'code'  : 'empty-payload',
        'title' : 'You supplied an empty payload for your PUT request. This will do nothing',
    }]}

def test_endpoint_methods_patch(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def patch(uuid=None, _id=None):
            assert uuid is None or isinstance(uuid, UUID.UUID)
            assert _id is None or isinstance(_id, int)
            return None
    sepiida.endpoints.add_resource(app, TestEndpoint)

    response = json_client.patch('/test-endpoint/')
    assert response.status_code == 404

    response = json_client.patch('/test-endpoint/123/')
    assert response.status_code == 204

def test_endpoint_methods_post(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def post():
            return {'content': 'c'}, 201, {'Location': 'somewhere'}
    sepiida.endpoints.add_resource(app, TestEndpoint)

    response = json_client.post('/test-endpoint/123/', json={})
    assert response.status_code == 404

    response = json_client.post('/test-endpoint/', json={})
    assert response.status_code == 201
    assert response.json == {'content': 'c'}
    assert response.headers['Content-Type'] == 'application/json'
    assert response.headers['Location'] == 'http://sepiida.service/somewhere'

def test_endpoint_methods_delete_with_defaults(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def delete(uuid=None, _id=None):
            assert uuid is None or isinstance(uuid, UUID.UUID)
            assert _id is None or isinstance(_id, int)
            return None
    sepiida.endpoints.add_resource(app, TestEndpoint)
    response = json_client.delete('/test-endpoint/')
    assert response.status_code == 204

    response = json_client.delete('/test-endpoint/123/')
    assert response.status_code == 204

def test_endpoint_methods_delete_without_defaults(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def delete(uuid):
            return uuid
    sepiida.endpoints.add_resource(app, TestEndpoint)
    response = json_client.delete('/test-endpoint/')
    assert response.status_code == 405
    expected = {
        'errors'    : [{
            'code'  : 'invalid-method',
            'title' : ('You attempted to request a DELETE on /test-endpoint/. '
                       'You must call DELETE on a specific resource rather than '
                       'the collection. You can do this by requesting a URL of the '
                       'pattern /test-endpoint/<uuid>/'),
        }]
    }
    assert response.json == expected

class FakeError(Exception):
    pass

@pytest.mark.parametrize('error, expected_status, expected_error', [
    (sepiida.errors.Specification(FakeError, 401, 'integrity-error', 'bad'), 401, {'code': 'integrity-error', 'title': 'bad'}),
    (sepiida.errors.Specification(FakeError, 401, 'integrity-error'), 401, {'code': 'integrity-error', 'title': 'Bad stuff'}),
    (sepiida.errors.Specification(FakeError, 401), 401, {'code': 'FakeError', 'title': 'Bad stuff'}),
    (sepiida.errors.Specification(FakeError,), 400, {'code': 'FakeError', 'title': 'Bad stuff'}),
])
def test_error_handling(app, error, expected_error, expected_status, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object()
        ERRORS = [error]
        @staticmethod
        def list():
            raise FakeError("Bad stuff")

    sepiida.endpoints.add_resource(app, TestEndpoint)
    response = json_client.get('/test-endpoint/')
    assert response.status_code == expected_status
    assert response.json == {'errors': [expected_error]}

def test_error_handling_inheritance(app, json_client):
    class TestEndpointParent(sepiida.endpoints.APIEndpoint):
        ERRORS = [sepiida.errors.Specification(Exception, 402, 'hit-exception-parent')]

    class TestEndpointChild(TestEndpointParent):
        ENDPOINT = '/test-endpoint-child/'
        ERRORS = [sepiida.errors.Specification(FakeError, 401, 'hit-exception-child')]

        @staticmethod
        def get(_id):
            if _id == 1:
                raise FakeError('Bad stuff')
            else:
                raise Exception("Bad payload")

    sepiida.endpoints.add_resource(app, TestEndpointChild)

    response = json_client.get('/test-endpoint-child/1/')
    assert response.status_code == 401
    assert response.json == {'errors': [{'code': 'hit-exception-child', 'title': 'Bad stuff'}]}

    response = json_client.get('/test-endpoint-child/2/')
    assert response.status_code == 402
    assert response.json == {'errors': [{'code': 'hit-exception-parent', 'title': 'Bad payload'}]}

def test_callback_uri(app, json_client):
    class TestCallback(sepiida.endpoints.APIEndpoint):
        SIGNATURE = sepiida.fields.Object(s={
            'foo'   : sepiida.fields.String(),
        })
        ENDPOINT = '/run/<uuid:_uuid>/callback/'
        @staticmethod
        def post(_uuid, payload): # pylint: disable=unused-argument
            return flask.make_response('this is some content yo for {}'.format(_uuid))

    sepiida.endpoints.add_resource(app, TestCallback)

    _uuid = UUID.uuid4()
    response = json_client.post('/run/{}/callback/'.format(_uuid), json={'foo': 'bar'})
    assert response.status_code == 200
    assert response.data.decode('utf-8') == 'this is some content yo for {}'.format(_uuid)

def test_error_handling_unrecognized_error(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object()
        @staticmethod
        def list():
            raise Exception("oh noes")

    app.config['DEBUG'] = False
    app.config['TESTING'] = False
    sepiida.endpoints.add_resource(app, TestEndpoint)
    response = json_client.get('/test-endpoint/')
    assert response.status_code == 500

def test_nested_resource(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object()

        @staticmethod
        def post(payload): # pylint: disable=unused-argument
            raise Exception("oh noes")

        @staticmethod
        def list():
            raise Exception("oh no")

        @staticmethod
        def get():
            raise Exception("oh")

    class TestEndpointNested(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/<int:nest_id>/nested/'
        SIGNATURE = sepiida.fields.Object()

        @staticmethod
        def post(nest_id, payload): # pylint: disable=unused-argument
            return

        @staticmethod
        def list(nest_id): # pylint: disable=unused-argument
            return []

        @staticmethod
        def get(nest_id, _id): # pylint: disable=unused-argument
            return {}

    sepiida.endpoints.add_resource(app, TestEndpoint)
    sepiida.endpoints.add_resource(app, TestEndpointNested)

    response = json_client.post('/test-endpoint/12/nested/', json={})
    assert response.status_code == 204
    response = json_client.get('/test-endpoint/12/nested/')
    assert response.status_code == 200
    assert response.json == {'resources': []}
    response = json_client.get('/test-endpoint/12/nested/34/')
    assert response.status_code == 200
    assert response.json == {}

def test_endpoint_name(app):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object()

        @staticmethod
        def get(_id):
            raise Exception(str(_id))

    sepiida.endpoints.add_resource(app, TestEndpoint, endpoint='mything')
    url = flask.url_for('mything', _id=12)
    assert url == '/test-endpoint/12/'

    uuid = UUID.uuid4()
    url = flask.url_for('mything', uuid=uuid)
    assert url == '/test-endpoint/{}/'.format(uuid)

def test_endpoint_auth_hook_no_action(app, configuration, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object()

        @classmethod
        def authenticate(cls):
            return

        @staticmethod
        def list():
            return

    sepiida.session.register_session_handlers(app, configuration.pao_root)
    sepiida.endpoints.add_resource(app, TestEndpoint)
    response = json_client.get('/test-endpoint/')
    assert response.status_code == 403

@pytest.mark.parametrize('error, expected_status', [
    (sepiida.errors.ResourceNotFound, 404),
    (sepiida.errors.InvalidPayload, 400),
    (sepiida.errors.Unauthorized, 403),
    (sepiida.errors.AuthenticationException, 403),
])
def test_endpoint_auth_hook_failed(app, configuration, json_client_session, error, expected_status):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object()

        @classmethod
        def authenticate(cls):
            raise error()

        @staticmethod
        def list():
            return

    sepiida.session.register_session_handlers(app, configuration.pao_root)
    sepiida.endpoints.add_resource(app, TestEndpoint)
    response = json_client_session.get('/test-endpoint/')
    assert response.status_code == expected_status

def test_endpoint_auth_hook_auth(app, configuration, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object()

        @classmethod
        def authenticate(cls):
            return {'user': 'kenny'}

        @staticmethod
        def list():
            return [sepiida.session.current_user()]

    sepiida.session.register_session_handlers(app, configuration.pao_root)
    sepiida.endpoints.add_resource(app, TestEndpoint)
    response = json_client.get('/test-endpoint/')
    assert response.status_code == 200
    assert response.json == {'resources': [{'user': 'kenny'}]}

@pytest.mark.parametrize('result', (
    lambda: None,
    lambda: 'foo',
    lambda: ['foo'],
    lambda: flask.make_response(''),
))
def test_endpoint_caching_headers_default(app, json_client, result):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'

        @staticmethod
        def list():
            return result()

    sepiida.endpoints.add_resource(app, TestEndpoint)
    response = json_client.get('/test-endpoint/')
    assert response.headers['Cache-Control'] == 'max-age=10'

@pytest.mark.parametrize('result', (
    lambda: None,
    lambda: 'foo',
    lambda: ['foo'],
    lambda: flask.make_response(''),
))
def test_endpoint_caching_headers_manual(app, json_client, result):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        CACHING = {'GET': 'flubber'}
        @staticmethod
        def list():
            return result()

    sepiida.endpoints.add_resource(app, TestEndpoint)
    response = json_client.get('/test-endpoint/')
    assert response.headers['Cache-Control'] == 'flubber'


def test_options_with_queryargs_no_signature(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def list():
            return None
    sepiida.endpoints.add_resource(app, TestEndpoint)
    response = json_client.options('/test-endpoint/?foo=bar')
    assert response.status_code == 204
