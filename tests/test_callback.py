import json

import sepiida.callback


def test_no_url():
    sepiida.callback.handle('https://callback.authentise.com/', None, None, None, None)

def test_no_method():
    sepiida.callback.handle(
        'https://callback.authentise.com/',
        'http://www.google.com',
        None,
        None,
        None)

def test_happy_post(httpretty):
    httpretty.register_uri(
        httpretty.POST,
        'https://callback.authentise.com/job/',
        status=201,
    )

    sepiida.callback.handle(
        'https://callback.authentise.com/',
        'http://www.google.com',
        'POST',
        'complete',
        'https://thing.authentise.com/thinger/123/')

    payload = {
        'status'    : 'complete',
        'uri'       : 'https://thing.authentise.com/thinger/123/',
    }

    expected = {
        'body'      : json.dumps(payload),
        'method'    : 'POST',
        'url'       : 'http://www.google.com',
    }
    assert json.loads(httpretty.last_request().body.decode('utf-8')) == expected
