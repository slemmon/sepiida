import sepiida.endpoints


def create_expected_payload_endpoint(app, signature, expected, return_on_post=False):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = signature
        @staticmethod
        def get(uuid=None, _id=None): # pylint: disable=unused-argument
            return expected

        @staticmethod
        def post(uuid=None, _id=None, payload=None): # pylint: disable=unused-argument
            assert payload == expected
            if return_on_post:
                return payload

        @staticmethod
        def put(uuid=None, _id=None, payload=None): # pylint: disable=unused-argument
            assert payload == expected

        @staticmethod
        def list(payload=None): # pylint: disable=unused-argument
            return expected if isinstance(expected, (list, tuple)) else [expected]
    sepiida.endpoints.add_resource(app, TestEndpoint, 'test_endpoint')
