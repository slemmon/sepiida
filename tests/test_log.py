import importlib
import logging

import sepiida.log


def test_log_without_sentry():
    logging.shutdown()
    importlib.reload(logging)
    assert len(logging.getLogger().handlers) == 0
    sepiida.log.setup_logging()
    assert len(logging.getLogger().handlers) == 1

def test_log_with_sentry():
    logging.shutdown()
    importlib.reload(logging)
    assert len(logging.getLogger().handlers) == 0
    sentry_dsn = 'https://a:b@app.getsentry.com/35300'
    sepiida.log.setup_logging(sentry_dsn=sentry_dsn)
    assert len(logging.getLogger().handlers) == 2
