import json
from uuid import uuid4

import pytest

import sepiida.groups


def test_search(app, httpretty, user):
    app.config['SEPIIDA_PAO_ROOT'] = 'https://user.service'
    groups = {'resources': ['admins', 'employees']}

    httpretty.register_uri(
        httpretty.GET,
        'https://user.service/memberships/?filter[user]={}'.format(user['uri']),
        body=json.dumps(groups),
        status_code=200,
    )
    with app.app_context():
        user_groups = sepiida.groups.search()
    assert user_groups == groups

def test_search_error(app, httpretty, user):
    app.config['SEPIIDA_PAO_ROOT'] = 'https://user.service'
    groups = {'resources': ['admins', 'employees']}

    httpretty.register_uri(
        httpretty.GET,
        'https://user.service/memberships/?filter[user]={}'.format(user['uri']),
        body=json.dumps(groups),
        status=400,
    )
    with app.app_context():
        with pytest.raises(Exception):
            sepiida.groups.search()


def test_has_any(app, mocker, httpretty):
    app.config['SEPIIDA_PAO_ROOT'] = 'https://user.service'
    group1 = 'https://user.service/groups/{}/'.format(uuid4())
    group2 = 'https://user.service/groups/{}/'.format(uuid4())
    group3 = 'https://user.service/groups/{}/'.format(uuid4())

    groups = {'resources': [{
        'uuid' : str(uuid4()),
        'group' : group1,
        'user' : 'https://user.service/user/{}/'.format(uuid4()),
    },
    {
        'uuid' : str(uuid4()),
        'group' : group2,
        'user' : 'https://user.service/user/{}/'.format(uuid4()),
    }]}
    mock = mocker.patch('sepiida.groups.search', return_value=groups)
    with app.app_context():
        httpretty.register_uri(
            httpretty.GET,
            'https://user.service/groups/?filter[name]=Ricoh',
            body=json.dumps({'resources': [{'uri': group1}]}),
            status_code=200,
        )
        assert sepiida.groups.has_any(['Ricoh']) is True
        httpretty.register_uri(
            httpretty.GET,
            'https://user.service/groups/?filter[name]=Authentise',
            body=json.dumps({'resources': [{'uri': group2}]}),
            status_code=200,
        )
        assert sepiida.groups.has_any(['Authentise']) is True
        httpretty.register_uri(
            httpretty.GET,
            'https://user.service/groups/?filter[name]=Samsung',
            body=json.dumps({'resources': [{'uri': group3}]}),
            status_code=200,
        )
        assert sepiida.groups.has_any(['Samsung']) is False
    assert mock.call_count == 3
