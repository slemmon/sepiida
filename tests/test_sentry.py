import flask

import sepiida.sentry


def test_add_support(configuration, user):
    # Most of our clients will not have an app context at the time they call this
    # function, so let's make sure that we test that way. This is why we have to build
    # the app ourselves right here - otherwise our test system creates an app context
    assert not flask.has_app_context()
    app = flask.Flask('sepiida')
    for prop, value in configuration.items():
        app.config[prop.upper()] = value

    @app.route('/test-endpoint/')
    def test(): # pylint: disable=unused-variable
        assert flask.current_app.config['RAVEN'].client.context['user']['uri'] == user['uri']
        return flask.make_response('')

    sepiida.session.register_session_handlers(app, configuration.pao_root)
    sentry_dsn = 'https://a:b@app.getsentry.com/35300'
    sepiida.sentry.add_sentry_support(app, sentry_dsn)
    with app.test_client() as test_client:
        with test_client.session_transaction() as session:
            session['user_uri'] = user['uri']
        response = test_client.get('/test-endpoint/')
        assert response.status_code == 200
