import pytest
import json
import time
import sepiida.memberships

@pytest.fixture
def memberships_payload():
    return {
        'group' : 'https://users.service/groups/123/',
        'user'  : 'https://users.service/users/123/',
    }

@pytest.mark.usefixtures("app")
@pytest.mark.parametrize('status, error', (
    (400, sepiida.memberships.MembershipsBadRequestError),
    (401, sepiida.memberships.MembershipsUnauthorizedError),
    (403, sepiida.memberships.MembershipsForbiddenError),
    (404, sepiida.memberships.MembershipsNotFoundError),
    (500, sepiida.memberships.MembershipsRequestError),
    (503, sepiida.memberships.MembershipsRequestError),
))
def test_send_request_failure(httpretty, status, error):
    sepiida.memberships.configure('https://pao.service')
    http_method = 'GET'
    uri = 'https://pao.service/123/'
    httpretty.register_uri(
        httpretty.GET,
        uri,
        status=status,
    )
    with pytest.raises(error) as e:
        sepiida.memberships._send_request(http_method, uri) #pylint: disable=protected-access
    assert e.value.args[0] == 'Failed Request, Response: {}, HTTPretty :), Payload: None'.format(status)

@pytest.mark.usefixtures("app")
def test_send_request_timeout(httpretty):
    sepiida.memberships.configure('https://pao.service')
    def request_callback(request, uri, headers): #pylint: disable=unused-argument
        time.sleep(1)
        return (200, {}, '{}')

    http_method = 'GET'
    uri = 'https://pao.service/123/'
    httpretty.register_uri(
        httpretty.GET,
        uri,
        body=request_callback,
    )
    with pytest.raises(sepiida.memberships.MembershipsConnectionError) as e:
        sepiida.memberships._send_request(http_method, uri, timeout=0.01) #pylint: disable=protected-access
    assert 'Read timed out' in str(e.value)

@pytest.mark.usefixtures("app")
def test_send_request_pao_root():
    sepiida.memberships.configure(None)
    #the reasone behind it is to reset the state so there aren't any leaks
    #throughout our tests
    http_method = 'GET'
    with pytest.raises(sepiida.memberships.MembershipsPaoRootError):
        sepiida.memberships._send_request(http_method) #pylint: disable=protected-access

def test_send_request_privileged(app, basic_auth, httpretty):
    def validate_body(request, dummy_uri, headers):
        success = request.headers['Authorization'] == basic_auth('api', app.config['API_TOKEN'])
        return 200, headers, json.dumps({'success': success})
    sepiida.memberships.configure('https://users.service')
    httpretty.register_uri(httpretty.POST,
        'https://users.service/memberships/',
        body=validate_body,
        status_code=201,
    )
    response = sepiida.memberships._send_request('POST', payload={}, privileged=True) # pylint: disable=protected-access
    assert response.ok
    assert response.json() == {'success': True}

def _setup_request_callback(httpretty, method, uri, status, payload=None, location=None):
    def request_callback(request, uri, headers): #pylint: disable=unused-argument
        if payload:
            assert json.loads(request.body.decode()) == memberships_payload()
        headers['Location'] = location or '123'
        return (status, headers, '')
    httpretty.register_uri(
        method,
        uri,
        body=request_callback,
    )

@pytest.mark.usefixtures("app")
def test_create(httpretty, memberships_payload): #pylint: disable=redefined-outer-name
    sepiida.memberships.configure('https://pao.service')
    uri = 'https://pao.service/memberships/'
    location = 'https://pao.service'
    _setup_request_callback(
        httpretty,
        method=httpretty.POST,
        uri=uri,
        status=204,
        payload=memberships_payload,
        location=location
    )

    response = sepiida.memberships.create(
        memberships_payload['group'],
        memberships_payload['user'],
    )
    assert response == location

@pytest.mark.usefixtures("app")
def test_delete(httpretty): #pylint: disable=redefined-outer-name
    sepiida.memberships.configure('https://pao.service')
    uri = 'https://pao.service/123/'
    location = 'https://pao.service'
    _setup_request_callback(httpretty,
        method=httpretty.DELETE,
        uri=uri,
        status=204,
        location=location
    )
    sepiida.memberships.delete(uri)
    assert httpretty.last_request().method == 'DELETE'
    assert httpretty.last_request().path == '/123/'

@pytest.mark.usefixtures("app")
def test_get(httpretty, memberships_payload): #pylint: disable=redefined-outer-name
    sepiida.memberships.configure('https://pao.service')
    uri = 'https://pao.service/123/'
    httpretty.register_uri(
        httpretty.GET,
        uri,
        body=json.dumps(memberships_payload),
        status=200,
    )
    response = sepiida.memberships.get(uri)
    assert response == memberships_payload

@pytest.mark.usefixtures("app")
@pytest.mark.parametrize('url, group, user', (
    ('filter[group]=resource_proxy', ['resource_proxy'], None),
    ('filter[user]=holder_proxy', None, ['holder_proxy']),
))
def test_search(httpretty, memberships_payload, url, group, user): #pylint: disable=redefined-outer-name
    sepiida.memberships.configure('https://pao.service')
    url = 'https://pao.service/memberships/?{}'.format(url)
    httpretty.register_uri(
        httpretty.GET,
        url,
        body=json.dumps({'resources' : [memberships_payload]}),
        status=200,
    )
    response = sepiida.memberships.search(group=group, user=user, privileged=True)
    assert response == [memberships_payload]
