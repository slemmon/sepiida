import json as JSON
from datetime import datetime, timedelta

import pytest

from sepiida import jwt


@pytest.mark.usefixtures("app")
def test_encode_success(external_jwt_helper, external_jwt):
    token = jwt.encode(payload=external_jwt["payload"], **external_jwt["body"])
    assert external_jwt_helper.decode(token) == external_jwt["body"]

def test_encode_specific_key(external_key, internal_key):
    params = {
        'payload'   : {'foo': 'bar'},
        'audience'  : 'someone',
        'issuer'    : 'me',
        'sub'       : 'The Santa Fe',
        'exp'       : 10,
        'iat'       : 10,
        'nbf'       : 10,
    }
    internal = jwt.encode(key=internal_key, **params)
    external = jwt.encode(key=external_key, **params)
    external_decoded = jwt.decode(external, verify=False)
    internal_decoded = jwt.decode(internal, verify=False)
    assert external_decoded == internal_decoded
    assert external != internal

@pytest.mark.parametrize(("claim", "verify"), [
    ("exp", False),
    ("nbf", False),
    ("iat", False),
    (None, True),
])
def test_decode_success(internal_jwt_helper, internal_jwt, claim, verify):
    internal_jwt["body"].pop(claim, None)
    token = internal_jwt_helper.basic_encode(internal_jwt["body"])
    body = jwt.decode(
        token,
        internal_jwt_helper.cert,
        audience=internal_jwt_helper.audience,
        issuer=internal_jwt_helper.issuer,
        payload=internal_jwt["payload"],
        verify=verify,
    )
    assert body == internal_jwt["body"]

@pytest.mark.parametrize("claim", ["exp", "iss", "iat", "nbf"])
def test_decode_missing_claim(internal_jwt_helper, internal_jwt, claim):
    internal_jwt["body"].pop(claim)
    token = internal_jwt_helper.basic_encode(internal_jwt["body"])

    with pytest.raises(jwt.MissingClaimError) as excinfo:
        jwt.decode(
            token,
            internal_jwt_helper.cert,
            audience=internal_jwt_helper.audience,
            issuer=internal_jwt_helper.issuer,
            payload=internal_jwt["payload"],
        )
    assert excinfo.value.title == 'Token is missing the "{}" claim'.format(claim)
    assert excinfo.value.error_code == 'jwt-missing-claim'

@pytest.mark.parametrize(("claim", "error", "error_code", "error_title"), [
    ("iss", jwt.InvalidIssuerError, "jwt-invalid-issuer", "Invalid issuer"),
    ("aud", jwt.InvalidAudienceError, "jwt-invalid-audience", "Invalid audience"),
    ("bdy", jwt.InvalidBodyError, "jwt-invalid-body", "Invalid Body"),
    ("exp", jwt.ExpiredSignatureError, "jwt-signature-expired", "Signature has expired"),
])
def test_decode_invalid_claim(internal_jwt_helper, internal_jwt, claim, error, error_code, error_title):
    if claim == "exp":
        utcnow = datetime.utcnow() - timedelta(seconds=1600)
        internal_jwt["body"][claim] = utcnow
    else:
        internal_jwt["body"][claim] = "42"

    token = internal_jwt_helper.basic_encode(internal_jwt["body"])

    with pytest.raises(error) as excinfo:
        jwt.decode(
            token,
            internal_jwt_helper.cert,
            audience=internal_jwt_helper.audience,
            issuer=internal_jwt_helper.issuer,
            payload=internal_jwt["payload"],
        )
    assert excinfo.value.title == error_title
    assert excinfo.value.error_code == error_code

@pytest.mark.usefixtures("app")
@pytest.mark.parametrize(("status", "error_code", "error_title", "response_body"), [
    (400, "jwt-invalid-issuer", "Invalid issuer", None),
    (401, "jwt-invalid-issuer", "Invalid issuer", None),
    (403, "jwt-invalid-issuer", "Invalid issuer", None),
    (404, "jwt-invalid-issuer", "Invalid issuer", None),
    (200, "jwt-invalid-issuer-response", "Invalid issuer response", None),
    (200, "jwt-invalid-issuer-secret", "Invalid issuer secret", { "secret": None }),
])
def test_validate_invalid_issuer(httpretty, internal_jwt_helper, internal_jwt, status, response_body, error_code, error_title):
    group_uri = "https://pao.service/groups/42/"
    internal_jwt["body"]["iss"] = group_uri
    token = internal_jwt_helper.basic_encode(internal_jwt["body"])

    httpretty.register_uri(
        httpretty.GET,
        group_uri,
        status=status,
        body=JSON.dumps(response_body),
    )

    with pytest.raises(jwt.InvalidIssuerError) as excinfo:
        jwt.validate(
            token,
            audience=internal_jwt_helper.audience,
            payload=internal_jwt["payload"],
        )
    assert excinfo.value.error_code == error_code
    assert excinfo.value.title == error_title

def test_decode_iss_success(internal_jwt):
    assert jwt.decode_iss(internal_jwt['jwt']) == internal_jwt["body"]["iss"]

def test_validate_invalid_token(internal_jwt_helper, internal_jwt):
    with pytest.raises(jwt.DecodeError) as excinfo:
        jwt.validate(
            'invalid token',
            audience=internal_jwt_helper.audience,
            payload=internal_jwt["payload"],
        )
    assert excinfo.value.error_code == "jwt-decode-error"
    assert excinfo.value.title == "Not enough segments"
