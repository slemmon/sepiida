import contextlib

import pytest

import sepiida.config


@contextlib.contextmanager
def override_configuration():
    previous = sepiida.config.get.cached
    yield
    sepiida.config.get.cached = previous

def test_configuration_is_autoused_and_globally_stored():
    config = sepiida.config.get()
    assert config

@pytest.mark.parametrize("option, content, expected", [
    (sepiida.config.Option(str, None),    'o: foo',               {'o': 'foo'}),
    (sepiida.config.Option(int, None),    'o: 2',                 {'o': 2}),
    (sepiida.config.Option(int, None),    'o: 2.3',               {'o': 2}),
    (sepiida.config.Option(float, None),  'o: 3.2',               {'o': 3.2}),
    (sepiida.config.Option(bool, None),   'o: true',              {'o': True}),
    (sepiida.config.Option(bool, None),   'o: 1',                 {'o': True}),
    (sepiida.config.Option(bool, None),   'o: 0',                 {'o': False}),
    (sepiida.config.Option(bool, None),   'o: false',             {'o': False}),
    (sepiida.config.Option(list, None),   'o:\n  - foo\n  - bar', {'o': ['foo', 'bar']}),
    (sepiida.config.Option(dict, None),   'o:\n  bar: baz',       {'o': {'bar': 'baz'}}),
])
def test_get(content, expected, mocker, option):
    specification = {
        'o'    : option
    }
    with override_configuration():
        with mocker.patch('sepiida.config._readfile', return_value=content):
            config = sepiida.config.load('/etc/authentise/test.yaml', specification)
        assert config == expected
        assert sepiida.config.get.cached == config

def test_extract_test_values():
    specification = {
        'o' : sepiida.config.Option(str, 'foo')
    }
    config = sepiida.config.extract_test_values(specification)
    assert config == {'o': 'foo'}


@pytest.mark.parametrize('option_type, value', [
    (int, 'foo'),
    (float, 'foo'),
    (list, 'foo'),
    (dict, 'foo'),
    (str, 1),
])
def test_extract_test_values_not_matching(option_type, value):
    with pytest.raises(Exception) as e:
        sepiida.config.Option(option_type, value)
    assert str(e.value) == 'You cannot specify a testing value of {} because it is not of type {}'.format(value, option_type.__name__)

@pytest.mark.parametrize("option_type, value", [
    (int, 'foo'),
    (bool, 'foo'),
    (float, 'foo'),
    (dict, 'foo'),
])
def test_incompatible_types(option_type, value):
    spec = {
        'o' : sepiida.config.Option(option_type, None)
    }
    content = "o: {}".format(value)
    with pytest.raises(Exception) as e:
        sepiida.config.parse(content, spec)
    expected = "The config option 'o' was provided with value '{}' which could not be parsed into a {}".format(value, option_type.__name__)
    assert str(e.value) == expected
