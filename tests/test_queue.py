def test_push_queue(queue):
    content = {
        'foo'   : 'bar'
    }
    queue.push('some-credentials', content)
    assert queue.sent({
        'credentials'   : 'some-credentials',
        'content'       : content
    })

def test_queue_messages(queue):
    queue.push('some-credentials', {'foo': 'bar'})
    for message in queue.messages():
        assert message.body == {
            'credentials'   : 'some-credentials',
            'content'       : {'foo': 'bar'},
        }
